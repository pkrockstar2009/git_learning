public class SchedulableApexExample1 implements Schedulable{
	public void execute(SchedulableContext ct)
    {
        Account a = new Account(name='Schedulable Account');
        insert a;
    }
}