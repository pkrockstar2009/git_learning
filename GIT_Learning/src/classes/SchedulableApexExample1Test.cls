@isTest
public class SchedulableApexExample1Test {
	@isTest
    public static void TestSchedulableApex()
    {
        Test.startTest();
        SchedulableApexExample1 a = new SchedulableApexExample1();
        String cron = '0 0 1 * * ?';
        Id jobId = System.schedule('ScheduleAccountExm1_Test', cron, a);
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where id = :jobId];
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2018-08-28 01:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
}