public class JSONSoapDemo {
	public static void sendJSONRequest()
    {
        String jsonRequest = '{     "name": "morpheus",     "job": "leader" }';
        Http http = new HTTP();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:UsersAPI');
        req.setMethod('POST');
        req.setBody(jsonRequest);
        HttpResponse res = http.send(req);
        System.debug(res.getBody());
        
        JSONParser parser = JSON.createParser(res.getBody());

    }
}