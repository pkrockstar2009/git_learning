public class Example4 {
	Account[] selectedActs;
    
    public Account[] getSelectedActs()
    {
        return selectedActs;
    }
    
    public PageReference showall()
    {
        selectedActs = [Select name, Type, Industry, PHone, Fax from Account];
        return null;
    }
    
    public PageReference showEnergy()
    {
        selectedActs = [Select name, Type, Industry, PHone, Fax from Account where Industry = 'Energy'];
        return null;
    }
}