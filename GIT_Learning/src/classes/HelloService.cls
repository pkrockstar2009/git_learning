global class HelloService {
    webservice static String sayHello(String name){
        Contact con = new Contact(LastName = name);
        insert con;        
        return 'Hello  ' + con.LastName+ ' ! : ' + con.Id;       
    }
}