public class q_class2 implements queueable{
	public void execute(QueueableContext con)
    {
        Account acc = [SELECT Id,name from account where name =: 'Queueable Account 2'];
        acc.name = 'Queueable Acc modified';
        update acc;
    }
}