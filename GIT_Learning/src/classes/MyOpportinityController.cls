public class MyOpportinityController {
	@AuraEnabled
    public static List<Opportunity> getOpptyList(List<Id> idList)
    {
        system.debug('id:'+idList);
        List<Opportunity> optyList = [SELECT NAME, Amount,StageName,ACCOUNTID FROM OPPORTUNITY WHERE ACCOUNTID IN :idList];
        system.debug('optyList:'+optyList);
        return optyList;
    }
}