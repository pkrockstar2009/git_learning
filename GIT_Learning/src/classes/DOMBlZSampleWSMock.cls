global class DOMBlZSampleWSMock implements HttpCalloutMock{
	global HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setBody('<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> 	<soapenv:Body> 		<ns1:getBankResponse xmlns:ns1="http://thomas-bayer.com/blz/"> 			<ns1:details> 				<ns1:bezeichnung>Bundesbank eh Braunschweig</ns1:bezeichnung> 				<ns1:bic>MARKDEF1270</ns1:bic> 				<ns1:ort>Hannover</ns1:ort> 				<ns1:plz>30002</ns1:plz> 			</ns1:details> 		</ns1:getBankResponse> 	</soapenv:Body> </soapenv:Envelope>');
    return res;
  }
}