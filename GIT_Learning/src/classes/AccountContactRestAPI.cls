@RestResource(urlMapping='/AccountContact/*')
global with sharing class AccountContactRestAPI {
    @HttpGet
    global static Account getAccount()
    {
        RestRequest req = RestContext.request;
        String AccountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account ac = [SELECT ID, NAME , (SELECT ID, NAME FROM CONTACTS) FROM ACCOUNT WHERE ID =: AccountId];
        return ac;
    }
}