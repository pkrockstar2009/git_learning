global class DailyLeadProcessor implements Schedulable{
    global void execute(SchedulableContext ctx)
    {
    	List<Lead> leads = new List<Lead>();
    	for(Lead L1:[SELECT ID, LeadSource from Lead where LeadSource = ''])
    	{	
    		L1.LeadSource = 'Dreamforce';
    		leads.add(L1);
    	}
    	Update leads;
    	
    }
}