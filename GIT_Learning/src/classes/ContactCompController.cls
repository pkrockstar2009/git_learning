public class ContactCompController {
	@auraEnabled
    public static List<Contact> findContact()
    {
        return [SELECT ID, FirstName, LastName, Phone, Email from Contact limit 10];
    }
}