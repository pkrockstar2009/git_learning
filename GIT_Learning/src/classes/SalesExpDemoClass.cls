public class SalesExpDemoClass {
	String keyword;
    List<Account> accList;
    
    public String getkeyword()
    {
        return keyword;
    }
    public List<Account> getaccList()
    {
        return accList;
    }
     public void setaccList()
    {
        accList = [SELECT ID,name,owner.firstName,type,industry,phone,fax,rating,website,AnnualRevenue,BillingAddress,ShippingAddress from Account limit 20];
    }
     public void setmyaccList()
    {
        system.debug('User :'+UserInfo.getUserId());
        accList = [SELECT ID,name,type,owner.FirstName,industry,phone,fax,rating,website,AnnualRevenue,BillingAddress,ShippingAddress from Account where ownerId =: UserInfo.getUserId() limit 20];
    }
    public void resetaccList()
    {
        system.debug('User :'+UserInfo.getUserId());
        accList = null;
    }
    public void setkeyword(String s)
    {
        keyword = s;
        System.debug('setKeyword: '+ keyword);
    }
    public PageReference search()
    {
        System.debug('Keyword: '+ keyword);
        if(keyword != '')
        {
        	accList = (Account[])[FIND :keyword returning Account(name,type,industry,phone,owner.firstName,fax,rating,website,AnnualRevenue,BillingAddress,ShippingAddress)][0];
        	if(accList.size() < 1)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'No Records Found.');
                ApexPages.addMessage(msg);
                return null;
            }
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'No Records Found.');
            ApexPages.addMessage(msg);
            return null;
        }
        return null;
    }
    public void quickUpdat()
    {
        update accList;
    }
    
    
}