public class Example5 {
	String keyword;
    List<Account> accList;
    
    public String getkeyword()
    {
        return keyword;
    }
    public List<Account> getaccList()
    {
        return accList;
    }
    public void setkeyword(String s)
    {
        keyword = s;
        System.debug('setKeyword: '+ keyword);
    }
    public PageReference search()
    {
        System.debug('Keyword: '+ keyword);
        accList = (Account[])[FIND :keyword returning Account(name,type,industry,phone,fax,rating,website,AnnualRevenue,BillingAddress,ShippingAddress)][0];
        return null;
    }
}