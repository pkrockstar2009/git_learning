global class MyContactsController {
	@AuraEnabled
    global static List<contact> getContactList(List<Id> accountIdList)
    {
        system.debug('AccID:'+ accountIdList);
        List<contact> conlist = [SELECT Id, FirstName, LastName,Phone, Email, AccountId from contact where accountId in : accountIdList];
        system.debug('ConList:'+ conlist);
        return conlist;
    }
}