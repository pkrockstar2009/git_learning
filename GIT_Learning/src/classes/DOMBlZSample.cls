public class DOMBlZSample {
	public static void sendRequest(String endPoint, String blzCode)
    {
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String serviceNS = 'http://thomas-bayer.com/blz/';
        String xsd = 'http://www.w3.org/2001/XMLSchema';
        String encoding = 'http://schemas.xmlsoap.org/soap/encoding/';

        //create XML document
        DOM.Document doc = new DOM.Document();
        
        //Create Envelope
        dom.XmlNode envelope = doc.createRootElement('Envelope',soapNS,'SOAP-ENV');
        envelope.setNameSpace('SOAP-ENC',encoding);
        envelope.setNameSpace('xsi', xsi);
        envelope.setNameSpace('xsd',xsd);
        
        //Create Body
        dom.XmlNode body = envelope.addChildElement('Body',soapNS,'SOAP-ENV');
        dom.XmlNode getBank = body.addChildElement('getBank', serviceNS, 'm');
        dom.XmlNode blz = getBank.addChildElement('blz', serviceNS, 'm');
        blz.addTextNode(blzCode);
        
        System.debug(doc.toXmlString());
        
        Http http = new http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/plain');
        req.setBodyDocument(doc);
        HttpResponse res = http.send(req);
        
        System.debug( res.getBodyDocument().toXMLString());
        
        envelope = res.getBodyDocument().getRootElement();
        System.debug(envelope.getChildElement('Body',soapNS).getChildElement('getBankResponse',serviceNS).getChildElement('details',serviceNS).getChildElement('ort',serviceNS).getText());
        System.assertEquals('Hannover', envelope.getChildElement('Body',soapNS).getChildElement('getBankResponse',serviceNS).getChildElement('details',serviceNS).getChildElement('ort',serviceNS).getText());

        
    }
}